package navarro.jarod.ui;

import java.io.*;

import navarro.jarod.logica.tl.*;
import sun.plugin.dom.core.CoreConstants;

public class Main {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException, Exception {
        boolean noSalir = true;
        int opcion;
        do {
            menu();
            out.println("Digite la opción que guste");
            opcion = Integer.parseInt(in.readLine());
            noSalir = ejecutarAccion(opcion);
        } while (noSalir);
    }

    public static void menu() {
        out.println("1. Registrar Cliente");
        out.println("2. Listar Clientes");
        out.println("3. Modificar Clientes");
        out.println("4. Borrar Cliente");
        out.println("5. Registrar Producto");
        out.println("6. Listar Producto");
        out.println("7. Modificar Producto");
        out.println("8. Borrar Producto");
        out.println("0. Salir");
    }

    public static boolean ejecutarAccion(int opcion) throws IOException, Exception {
        boolean noSalir = true;
        switch (opcion) {
            case 1:
                registrarCliente();
                break;
            case 2:
                listarClientes();
                break;
            case 3:
                modificarClinete();
                break;
            case 4:
                eliminarCliente();
                break;
            case 5:
                registrarProducto();
                break;
            case 6:
                listarProductos();
                break;
            case 7:
                modificarProducto();
                break;
            case 8:
                eliminarProducto();
                break;
            case 0:
                noSalir = false;
                out.println("Gracias por usar el probrama");
        }
        return noSalir;
    }

    public static void registrarCliente() throws IOException, Exception {
        String nombreCompleto;
        String cedula;
        int edad;
        String direccion;
        out.println("Digite el nombre completo del cliente");
        nombreCompleto = in.readLine();
        out.println("Digite el numero de cedula del cliente");
        cedula = in.readLine();
        out.println("Digite la edad del cliente");
        edad = Integer.parseInt(in.readLine());
        out.println("Digite la dirección del clinete");
        direccion = in.readLine();
        gestor.registrarCliente(nombreCompleto, cedula, edad, direccion);
    }

    public static void modificarClinete() throws IOException, Exception {
        String nombreCompleto;
        String cedula;
        int edad;
        String direccion;
        out.println("Digite la cedula del cliente que desea modificar");
        cedula = in.readLine();
        out.println("Digite el nuevo nombre completo");
        nombreCompleto = in.readLine();
        out.println("Digite la nueva edad");
        edad = Integer.parseInt(in.readLine());
        out.println("Digite la nueva dirección");
        direccion = in.readLine();
        gestor.editarCliente(cedula, nombreCompleto, edad, direccion);
    }

    public static void eliminarCliente() throws IOException, Exception {
        String cedula;
        out.println("Digite la cedula del cliente que desea eliminar");
        cedula = in.readLine();
        gestor.eliminarCliente(cedula);
    }

    public static void listarClientes() throws IOException, Exception {
        for (String dato : gestor.listarClientes()) {
            out.println(dato);
        }
    }

    public static void registrarProducto() throws IOException, Exception {
        String codigo;
        String descripcion;
        double precio;
        String categoria;
        out.println("Digite el codigo del producto");
        codigo = in.readLine();
        out.println("Digite la descripcion del producto");
        descripcion = in.readLine();
        out.println("Digite el precio del producto");
        precio = Double.parseDouble(in.readLine());
        out.println("Digite la categoria del producto");
        categoria = in.readLine();
        gestor.registrarProducto(codigo, descripcion, precio, categoria);
    }

    public static void modificarProducto()throws IOException, Exception{
        String codigo;
        String descripcion;
        double precio;
        String categoria;
        out.println("Digite el codigo del producto que desea modificar");
        codigo = in.readLine();
        out.println("Digite la nueva descripcion del producto");
        descripcion = in.readLine();
        out.println("Digite el nuevo precio del producto");
        precio = Double.parseDouble(in.readLine());
        out.println("Digite la nueva categoria del producto");
        categoria = in.readLine();
        gestor.editarProducto(codigo, descripcion, precio, categoria);
    }

    public static void eliminarProducto()throws IOException,Exception{
        out.println("Digite el codigo del producto que desea eliminar");
        String codigo = in.readLine();
        gestor.eliminarProducto(codigo);
    }

    public static void listarProductos()throws IOException,Exception{
        for(String dato : gestor.listarProductos()){
            out.println(dato);
        }
    }
}

